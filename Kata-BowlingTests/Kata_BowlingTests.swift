//
//  Kata_BowlingTests.swift
//  Kata-BowlingTests
//
//  Created by Matt Jacquet on 3/16/21.
//

import XCTest
@testable import Kata_Bowling

class Kata_BowlingTests: XCTestCase {

    func testGetRightRollType() {
        let scoringManager = ScoringManager()
        XCTAssertEqual(scoringManager.getRollType(value: "-"), .miss)
        XCTAssertEqual(scoringManager.getRollType(value: "/"), .spare);
        XCTAssertEqual(scoringManager.getRollType(value: "x"), .strike);
        XCTAssertEqual(scoringManager.getRollType(value: "5"), .pinsHit);
        XCTAssertEqual(scoringManager.getRollType(value: "+"), .unknown);
    }
    
    func testGivenSequenceGivesGoodFrameNumber() {
        let scoringManager = ScoringManager()
        scoringManager.setFramesFromSequence(sequence: "--")
        XCTAssertEqual(scoringManager.frames.count, 1)
        
        scoringManager.frames.removeAll()
        scoringManager.setFramesFromSequence(sequence: "")
        XCTAssertEqual(scoringManager.frames.count, 0)
        
        scoringManager.frames.removeAll()
        scoringManager.setFramesFromSequence(sequence: "5/-8")
        XCTAssertEqual(scoringManager.frames.count, 2)
        
        scoringManager.frames.removeAll()
        scoringManager.setFramesFromSequence(sequence: "457")
        XCTAssertEqual(scoringManager.frames.count, 2)
    }
    
    func testMissRollScore() {
        let scoringManager = ScoringManager()
        scoringManager.setFramesFromSequence(sequence: "--")
        XCTAssertEqual(scoringManager.frames.first?.totalScore, 0)
    }
    
    func testSpareRollScoreWithoutBonus() {
        let scoringManager = ScoringManager()
        scoringManager.setFramesFromSequence(sequence: "5/")
        XCTAssertEqual(scoringManager.frames.first?.totalScore, 15)
    }
    
    func testStrikeRollScore() {
        let scoringManager = ScoringManager()
        scoringManager.setFramesFromSequence(sequence: "x")
        XCTAssertEqual(scoringManager.frames.first?.totalScore, 10)
    }
    
    func testPinshitRollScore() {
        let scoringManager = ScoringManager()
        scoringManager.setFramesFromSequence(sequence: "34")
        XCTAssertEqual(scoringManager.frames.first?.totalScore, 7)
    }
    
    func testStrikeScoreWithBonus() {
        let scoringManager = ScoringManager()
        scoringManager.getTotalScore(for: "xxx5-5-")
        XCTAssertEqual(scoringManager.score, 80)
        
        scoringManager.resetGame()
        scoringManager.getTotalScore(for: "x5-5-")
        XCTAssertEqual(scoringManager.score, 25)
    }
    
    func testSpareScoreWithBonus() {
        let scoringManager = ScoringManager()
        scoringManager.resetGame()
        scoringManager.getTotalScore(for: "5/5/5/54")
        XCTAssertEqual(scoringManager.score, 54)
    }
    
    func testIsIntermediate() {
        let scoringManager = ScoringManager()
        scoringManager.getTotalScore(for: "x5")
        XCTAssertEqual(scoringManager.isIntermediateState, true)
        
        scoringManager.resetGame()
        scoringManager.getTotalScore(for: "xxx5-5-")
        XCTAssertEqual(scoringManager.isIntermediateState, false)
    }
    
    func testUnkownInout() {
        let scoringManager = ScoringManager()
        
        let sequence = "t"
        for roll in sequence {
            scoringManager.addRollToFrame(roll: "\(roll)")
        }
        
        if let firstRoll = scoringManager.frames[0].firstRoll {
            let firstRollType = scoringManager.getRollType(value: firstRoll)
            XCTAssertEqual(firstRollType, .unknown)
        }
    }
}
