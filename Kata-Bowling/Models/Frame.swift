//
//  Frame.swift
//  Kata-Bowling
//
//  Created by Matt Jacquet on 3/17/21.
//

import Foundation

class Frame {
    var firstRoll: String?
    var secondRoll: String?
    var totalScore = 0
    
    func isStrike() -> Bool { return firstRoll == "x" }
    
    func isSpare() -> Bool { return secondRoll == "/" }
    
    func isComplete() -> Bool { return firstRoll != nil && secondRoll != nil }
}
