//
//  ScoringSymbols.swift
//  Kata-Bowling
//
//  Created by Matt Jacquet on 3/17/21.
//

enum ScoringSymbols: String {
    case strike = "x" 
    case spare = "/"
    case miss = "-"
    case pinsHit = ""
    case unknown = "unknown"
}
