//
//  ViewController.swift
//  Kata-Bowling
//
//  Created by Matt Jacquet on 3/16/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var sequenceTextField: UITextField!
    
    var scoringManager: ScoringManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        scoringManager = ScoringManager()
    }
    
    @IBAction func calculateAction() {
        scoringManager.resetGame()
        
        if let value = sequenceTextField.text {
        scoringManager.getTotalScore(for: value)
            if scoringManager.isIntermediateState {
                resultLabel.text = "Waiting for another roll"
            }else {
                resultLabel.text = "Score: \(scoringManager.score)"
            }
        }
    }
}

