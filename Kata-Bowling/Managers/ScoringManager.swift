//
//  ScoringManager.swift
//  Kata-Bowling
//
//  Created by Matt Jacquet on 3/17/21.
//

import Foundation


class ScoringManager {
    
    var frames = [Frame]()
    var score: Int = 0
    var isIntermediateState = false
    
    func getTotalScore(for sequence: String) {
        setFramesFromSequence(sequence: sequence)
        setTotalScore()
        removeSparesFirstRoll()
        addBonuses()
        print("Score: \(score)")
    }
    
    func getRollType(value: String) -> ScoringSymbols {
        if Int(value) != nil {
            return .pinsHit
        }else if value == ScoringSymbols.strike.rawValue {
            return .strike
        }else if value == ScoringSymbols.spare.rawValue {
            return .spare
        }else if value == ScoringSymbols.miss.rawValue {
            return .miss
        }
        return .unknown
    }
    
    func setFramesFromSequence(sequence: String) {
        for roll in sequence {
            addRollToFrame(roll: "\(roll)")
        }
        
        for frame in frames {
            setFrameScore(frame: frame)
        }
    }
    
    func addRollToFrame(roll: String) {
        if frames.isEmpty {
            createNewFrame(roll: roll)
        }else if frames[frames.count-1].secondRoll != nil {
            createNewFrame(roll: roll)
        }else if frames[frames.count-1].firstRoll != nil && frames[frames.count-1].isStrike() == false {
            frames[frames.count-1].secondRoll = roll
        }else if frames[frames.count-1].firstRoll != nil && frames[frames.count-1].isStrike() {
            createNewFrame(roll: roll)
        }
    }
    
    func createNewFrame(roll: String) {
        let frame = Frame()
        frame.firstRoll = roll
        frames.append(frame)
    }
    
    func setFrameScore(frame: Frame) {
        
        if let firstRoll = frame.firstRoll {
            let firstRollType = getRollType(value: firstRoll)
            addRollScore(frame: frame, rollType: firstRollType, rollValue: firstRoll)
        }
        
        if let secondRoll = frame.secondRoll {
            let secondRollType = getRollType(value: secondRoll)
            addRollScore(frame: frame, rollType: secondRollType, rollValue: secondRoll)
        }
    }
    
    func addRollScore(frame: Frame, rollType: ScoringSymbols, rollValue: String) {
        switch rollType {
        case .pinsHit:
            guard let pins = Int(rollValue) else { return }
            frame.totalScore += pins
        case .miss:
            break
        case .spare, .strike:
            frame.totalScore += ScoringConst.MAX_VALUE
        case .unknown:
            print("invalid input")
        }
    }
    
    func setTotalScore() {
        for frame in frames  {
            score += frame.totalScore
        }
    }
    
    func removeSparesFirstRoll() {
        for frame in frames {
            if frame.secondRoll == ScoringSymbols.spare.rawValue {
                if let pins = Int(frame.firstRoll!) {
                    score -= pins
                }
            }
        }
    }
    
    func addBonuses() {
        let maxIndex = frames.count-1
        
        for i in 0...maxIndex {
            
            let nextFrameIndex = i+1
            let afterNextFrameIndex = i+2
            
            if frames[i].isStrike() {
                addStrikeBonuses(maxIndex: maxIndex, nextFrameIndex: nextFrameIndex, afterNextFrameIndex: afterNextFrameIndex)
            }
            
            if frames[i].isSpare() {
                addSpareBonuse(maxIndex: maxIndex, nextFrameIndex: nextFrameIndex, afterNextFrameIndex: afterNextFrameIndex, currentFrame: frames[i])
            }
        }
    }
    
    func addStrikeBonuses(maxIndex: Int, nextFrameIndex: Int, afterNextFrameIndex: Int) {
        if nextFrameIndex <= maxIndex {
            let nextFrame = frames[nextFrameIndex]
            score += nextFrame.totalScore
            
            if nextFrame.isStrike() {
                if afterNextFrameIndex <= maxIndex {
                    score += frames[afterNextFrameIndex].totalScore
                }
            }else {
                if !nextFrame.isComplete() {
                    isIntermediateState = true
                }
            }
        }
    }
    
    func addSpareBonuse(maxIndex: Int, nextFrameIndex: Int, afterNextFrameIndex: Int, currentFrame: Frame) {
        if nextFrameIndex <= maxIndex {
            if Int(currentFrame.firstRoll!) != nil {
                score += Int(frames[nextFrameIndex].firstRoll!)!
            }
        }
    }
    
    func resetGame() {
        frames = [Frame]()
        isIntermediateState = false
        score = 0
    }
}
